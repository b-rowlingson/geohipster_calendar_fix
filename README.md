# geohipster_calendar_fix

Whoops. The 2024 GeoHipster calendar got printed with 
2023 dates on it. All the weekdays wrong. Let's all 
support Randall in this difficult time by buying more
calendars once he's fixed it up.

https://www.geohipster.com/2023/11/21/2024-geohipster-calendar-2/

But what about the ones already out there? How do we fix
them? Just print out the corrective labels here onto 
sticky label paper, or use glue, and stick them over the
wrong headers on each month.

You might also need to add "29" to a gray box in February,
since 2023 wasn't a leap year but 2024 is.

The little next and previous month views on each page are 
wrong too, but I can't be troubled making corrections for
those!

Repo contains two SVG files prepared in inkscape with 
the data, a script to create the converted PDFs, and the 
converted individual PDFs and a merged one ready to print
(remember single sided!), cut, and stick.

All content CC-0 - free to use, remix, modify for any
purpose and no need to credit me. No warranty.

Or leave your calendar uncorrected and who knows, maybe
it'll be the "Inverted Jenny" of GeoHipster Calendars!

https://en.wikipedia.org/wiki/Inverted_Jenny
